# Player Keys constants

Constants for the state of player input. Values are set up as a 4-way boolean, causing press/hold events to evaluate to true, and release/up events to evaluate to false.

| Constant | Value | Description |
| --- | --- | --- |
| KEYS_PRESSED | 1 | The frame the key is pressed. |
| KEYS_DOWN | true | Every frame the key is held. |
| KEYS_UNPRESSED | nil | The frame the key is released. |
| KEYS_UP | false | Every frame the key is not pressed. |